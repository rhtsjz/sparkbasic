package io.github.rhsjz.examples.hw

import org.apache.spark.{SparkContext, SparkConf}

/**
  * Created by zsj on 16-1-11.
  */
object WordCount extends App {
  val helloMaster = "local"

  var conf = new SparkConf().setAppName("hello").setMaster(helloMaster)
  var spark = new SparkContext(conf)

  val textFile = spark.textFile("hdfs://test4-7:9000/log/test/zcoolpage.access.log-2016-01-01.gz")

  val counts = textFile.flatMap(line => line.split(" "))
    .map(word => (word, 1))
    .reduceByKey(_+_)

  counts.saveAsTextFile("hdfs://test4-7:9000/zsj/abc.txt")

}
