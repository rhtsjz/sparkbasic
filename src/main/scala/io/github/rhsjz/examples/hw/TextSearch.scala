package io.github.rhsjz.examples.hw

import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by zsj on 16-1-11.
  */
object TextSearch extends App{

  val helloMaster = "local"

  var conf = new SparkConf().setAppName("hello").setMaster(helloMaster)
  var spark = new SparkContext(conf)

  val textFile = spark.textFile("hdfs://test4-7:9000/log/test/zcoolpage.access.log-2016-01-01.gz")
  println(textFile.count())
  //val errors = textFile.filter(line => line.contains("REQUEST"))
  //println(errors.count()+"===============")

}
