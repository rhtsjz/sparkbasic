package io.github.rhsjz.examples.hw

import org.apache.spark.{SparkContext, SparkConf}

/**
  * Created by zsj on 16-1-11.
  */
object EstimatingPi extends App {
  val helloMaster = "local"

  var conf = new SparkConf().setAppName("hello").setMaster(helloMaster)
  var spark = new SparkContext(conf)
  val NUM_SAMPLES = 10
  val count = spark.parallelize(1 to NUM_SAMPLES).map{i =>
    val x = Math.random()
    val y = Math.random()
    if (x*x + y*y < 1) 1 else 0
  }.reduce(_+_)

  println("Pi is roughly " + 4.0 * count / NUM_SAMPLES)

}
