package io.github.rhsjz.basic.basics.classes

/**
  * Created by zsj on 16-1-19.
  */
class ScientificCalculator(brand: String) extends Calculator(brand){
  def log(m: Double, base: Double) = math.log(m) / math.log(base)

}
