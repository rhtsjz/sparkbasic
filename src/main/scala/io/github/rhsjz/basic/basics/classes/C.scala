package io.github.rhsjz.basic.basics.classes

/**
  * Created by zsj on 16-1-19.
  */
class C {
  var acc = 0
  def minc = { acc += 1}
  val finc = { () => acc += 1}
}
