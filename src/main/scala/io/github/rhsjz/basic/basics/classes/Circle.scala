package io.github.rhsjz.basic.basics.classes

/**
  * Created by zsj on 16-1-19.
  */
class Circle(r:Int) extends Shape{
  override def getArea(): Int = {
    r * r * 3
  }
}
