package io.github.rhsjz.basic.basics

/**
  * Created by zsj on 16-1-20.
  */
object Basic {

  def main(args: Array[String]) {
    // 值 Values
    val two = 1+1
    println("two: " + two)
    // 变量 Variables
    var name = "steve"
    name = "marius"
    println("name: " + name)


    // 函数 Functions
    def addOne(m: Int): Int = m+1
    val three = addOne(2)
    println("three: " + three)

    def four() = 2+2
    println("four(): " + four())
    println("four: " + four)

    //匿名函数 Anonymous Functions
    //(x:Int) => x+1
    val anonymousAddOne = (x: Int) => x+1
    println("anonymousAddOne: " + anonymousAddOne)
    println("anonymousAddOne(3): " + anonymousAddOne(3))

    def timesTwo(i: Int): Int = {
      println("hello world: " + i)
      i*2
    }
    println("timesTwo: "+timesTwo(3))
    val anonymousTimesTwo = {(x: Int) =>
      println("hello world: " + x)
      x*2
    }
    println("anonymousTimesTwo(6): " + anonymousTimesTwo(6))


    // 部分应用（Partial application）
    def adder(m:Int, n:Int):Int = {
      m+n
    }
    val add2 = adder(_:Int, 2)
    println("add2(3): " + add2(3))


    // 柯里化函数 Curried functions
    def multiply(m: Int)(n: Int): Int = {
      m * n
    }
    println("multiply(3)(2): " + multiply(3)(2))

    val timesThree = multiply(3)_
    println("timesThree: " + timesThree)
    println("timesThree(4): " + timesThree(4))

    val curriedAdd = (adder _).curried
    println("curriedAdd: " + curriedAdd)
    val add3 = curriedAdd(3)
    println("add3(6): " + add3(6))


    // 可变长度参数 Variable length arguments
    def capitalizeAll(args: String*) = {
      args.map { arg =>
        arg.capitalize
      }
    }
    println("capitalizeAll(\"rarity\", \"applejack\"): " + capitalizeAll("rarity", "applejack"))
  }

}
