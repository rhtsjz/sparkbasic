package io.github.rhsjz.basic.basics.classes

/**
  * Created by zsj on 16-1-19.
  */
abstract class Shape {
  def getArea():Int // subclass should define this

}
