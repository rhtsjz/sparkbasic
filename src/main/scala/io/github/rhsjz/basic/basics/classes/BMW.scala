package io.github.rhsjz.basic.basics.classes

import io.github.rhsjz.basic.basics.classes.Shiny

/**
  * Created by zsj on 16-1-19.
  */
class BMW extends Car with Shiny{
  override val brand: String = "BWM"
  override val shineRefraction: Int = 12
}
