package io.github.rhsjz.basic.basics

/**
  * Created by zsj on 16-1-12.
  */
object Collection {

  def main(args: Array[String]) {

    // 使用： 优先使用不可变集合，使用集合类型缺省的构造函数
    val seq = Seq(1, 2, 3)
    val set = Set(1, 2, 3)
    val map = Map(1->"one", 2->"two", 3->"three")

    // 风格：函数式编程鼓励使用流水线转换将一个不可变的集合塑造为想要的结果。
    // 这常常会有非常简明的方案，但也容易迷糊读者——很难领悟作者的意图，或跟踪所有隐含的中间结果。
    // 例如，我们想要从一组语言中汇集不同的程序语言的投票，按照得票的顺序显示(语言，票数)：
    val votes = Seq(("scala", 1), ("java", 4), ("scala", 10), ("scala", 1), ("python", 10))
    val orderdVotes = votes
      .groupBy(_._1)
      .map { case (which, counts) =>
        (which, counts.foldLeft(0)(_+_._2))
      }.toSeq
      .sortBy(_._2)
      .reverse

    // 上面的代码简洁并且正确，但几乎每个读者都不能理解作者的原本意图。一个策略是声明中间结果和参数：
    val votesByLang = votes groupBy{ case (lang, _) => lang}
    val sumByLang = votesByLang map {case (lang, counts) =>
        val countsOnly = counts map { case (_, count) => count}
      (lang, countsOnly.sum)
    }
    val orderdVotes1 = sumByLang.toSeq
      .sortBy { case (_, count) => count}
      .reverse

  }

}
