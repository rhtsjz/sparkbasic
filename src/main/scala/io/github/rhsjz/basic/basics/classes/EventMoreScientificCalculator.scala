package io.github.rhsjz.basic.basics.classes

/**
  * Created by zsj on 16-1-19.
  */
class EventMoreScientificCalculator(brand: String) extends ScientificCalculator(brand) {
  def log(m: Int): Double = log(m, math.exp(1))

}
