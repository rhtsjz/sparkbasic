package io.github.rhsjz.basic

import io.github.rhsjz.basic.basics.classes.{C, Calculator}
import io.github.rhsjz.basic.basics2.{AddOne, Bar}

/**
  * Created by zsj on 16-1-19.
  */
object ZScala {
  def main(args: Array[String]) {
    val calc = new Calculator("HP")
    val sum = calc.add(3, 6)
    println(sum)
    println(calc.color)

    val c = new C
    c.minc
    println(c.acc)
    println(c.minc)
    c.finc()
    println(c.acc)
    println(c.finc())

    val b = Bar.apply("s")
    println(b.f)

    val plusOne = new AddOne()
    println(plusOne(3))

  }

  def bigger(o: Any):Any = {
    o match {
      case i: Int if i < 0 => i-1
      case i: Int => i+1
      case d: Double if d < 0.0 => d-0.1
      case d: Double => d+0.1
      case text: String => text + "s"
    }
  }

}
