package io.github.rhsjz.basic.basics2

/**
  * Created by zsj on 16-1-19.
  */
object addOne extends ((Int) => Int){
  override def apply(v1: Int): Int = v1 + 1
}
