package io.github.rhsjz.basic.basics2

/**
  * Created by zsj on 16-1-19.
  */
object Timer {
  var count = 0

  def currentCount(): Long = {
    count += 1
    count
  }

  def main(args: Array[String]) {
    println(Timer.currentCount())
  }

}
