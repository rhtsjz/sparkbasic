package io.github.rhsjz.basic.basics2

/**
  * Created by zsj on 16-1-19.
  */
class AddOne extends ((Int) => Int) {
  override def apply(v1: Int): Int = v1 + 1
}
