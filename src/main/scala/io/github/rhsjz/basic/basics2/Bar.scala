package io.github.rhsjz.basic.basics2

/**
  * Created by zsj on 16-1-19.
  */
class Bar(foo:String) {
  val f = foo
}

object Bar{
  def apply(foo: String) = new Bar(foo)
}
