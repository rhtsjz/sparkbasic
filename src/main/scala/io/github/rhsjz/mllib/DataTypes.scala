package io.github.rhsjz.mllib

import org.apache.spark.mllib.linalg.distributed.RowMatrix
import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.mllib.linalg.{Matrices, Matrix, Vector, Vectors}
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.rdd.RDD

/**
  * Created by zsj on 16-1-12.
  */
object DataTypes {

  def main(args: Array[String]) {
    val helloMaster = "local"

    val conf = new SparkConf().setAppName("hello").setMaster(helloMaster)
    val sc = new SparkContext(conf)


    /*
    Local vector
     */

    // create a dense vector (1.0, 0.0, 3.0).
    val dv: Vector = Vectors.dense(1.0, 0.0, 3.0)

    // create a sparse vector (1.0, 0.0, 3.0) by specifying its indices and values
    // corresponding to nonzero entries.
    val sv1: Vector = Vectors.sparse(3, Array(0, 2), Array(1.0, 3.0))
    // create a sparse vector (1.0, 0.0, 3.0) by specifying its nonzero entries.
    val sv2: Vector = Vectors.sparse(3, Seq((0, 1.0), (2, 3.0)))

    /*
    Labeled point
     */

    // create a labeled point with a positive label and a dense feature vector.
    val pos = LabeledPoint(1.0, Vectors.dense(1.0, 0.0, 3.0))
    // create a labeled point with a negative label and a sparse feature vector.
    val neg = LabeledPoint(0.0, Vectors.sparse(3, Array(0, 2), Array(1.0, 3.0)))
    // Sparse data
    //val examples: RDD[LabeledPoint] = MLUtils.loadLibSVMFile(sc, "")
    
    /*
    Local matrix
     */
    // create a dense matrix ((1.0, 2.0), (3.0, 4.0), (5.0,6.0))
    val dm: Matrix = Matrices.dense(3,2,Array(1.0, 3.0, 5.0, 2.0, 4.0, 6.0))
    // create a sparse matrix ((9.0, 0.0), (0.0, 8.0), (0.0,6.0))
    val sm: Matrix = Matrices.sparse(3, 2, Array(0, 1, 3), Array(0, 2, 1), Array(9, 6, 8))

    val smRows = sm.numRows
    val smCols = sm.toArray
    println("===========================")
    println(smRows)
    println(smCols(0))
    println(smCols(1))
    println(smCols(2))
    println(smCols(3))
    println(smCols(4))
    println(smCols(5))

    /*
    Distributed matrix
     */
    // RowMatrix
    val rows: RDD[Vector] = null
    // create a rowmatrix from an RDD[Vector].
    val mat: RowMatrix = new RowMatrix(rows)

    // Get its size.
    val m = mat.numRows()
    val n = mat.numCols()

    // QR decomposition
    val qrResult = mat.tallSkinnyQR(true)

  }

}
