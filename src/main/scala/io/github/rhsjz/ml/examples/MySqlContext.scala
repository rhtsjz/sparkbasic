package io.github.rhsjz.ml.examples

import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkContext, SparkConf}

/**
  * Created by zsj on 16-1-19.
  */
object MySqlContext {
  val helloMaster = "local"
  val conf = new SparkConf().setAppName("hello").setMaster(helloMaster)
  val sc = new SparkContext(conf)

  val sqlContext = new SQLContext(sc)

  def apply() = sqlContext
}
