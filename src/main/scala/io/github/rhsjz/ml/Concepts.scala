package io.github.rhsjz.ml

/**
  * Created by zsj on 16-1-13.
  */
object Concepts {
  def main(args: Array[String]) {
    /*
     * Spark 标准化了机器学习算法的 API ,这使得联合多个算法到一个管道或工作流变得容易.
     * 这一节覆盖了 Spark ML API 中的一些关键概念, (pipeline)管道概念的灵感来自 scikit-learn 项目
     */

    // DataFrame 一个非常厉害的机器学习的数据集

    // Transformer 一个可以将一个 DataFrame 转换成另一个 DataFrame 的抽象
    // transform
    // Estimator 一个算法, 它可以适用于一个 DataFrame 然后产生一个 Transformer 抽象了学习算法
    // fit
    // Pipeline 一个 Pipeline 将好多个 Transformers 和 Estimators 链接起来组成一个 ML 工作流
    //
    // Parameter 所有的 Transformers 和 Estimators 现在共享一个共同的 API 对于指定的参数
  }

}
