package io.github.rhsjz

import org.apache.spark.{SparkConf, SparkContext}

/**
 * Hello world!
 *
 */
object MainApp extends App {
  println( "Hello World!" )

  val helloMaster = "local"

  val conf = new SparkConf().setAppName("hello").setMaster(helloMaster)
  val sc = new SparkContext(conf)


  /// Parallelized Collections
  val data = Array(1, 2, 3, 4, 5)
  var distData = sc.parallelize(data)


  /// External Datasets
  val dataFilePath = "target/classes/data.txt"
  val distFile = sc.textFile(dataFilePath)

  /// RDD Operations
  // Basics
  val lines = sc.textFile(dataFilePath)
  val lineLengths = lines.map(s => s.length)
  val totalLength = lineLengths.reduce((a, b) => a + b)
  println(lineLengths)
  println(totalLength)


  /// Understanding closures

  // foreach()
  var counter = 0
  var rdd = sc.parallelize(data)

  // Wrong: Don't do this!!!
  rdd.foreach(x => counter += x)

  println("Counter value: " + counter)

  // Printing elements of an RDD
  //rdd.collect().foreach(x => println(x))
  rdd.take(100).foreach(x => println(x))


  /// Working with Key-Value Pairs
  val pairs = lines.map(s => (s, 1))
  val counts = pairs.reduceByKey((a, b) => a + b)
  println(counts+"====================")


  /// Transformations
  /// Actions

  /// Shuffle operations

  /// RDD Persistence

  /// Shared Variables
  // Broadcast Variables
  var bArray = Array(1, 2, 3)
  val broadcastVar = sc.broadcast(bArray)
  broadcastVar.value
  // Accumulators
  val myAccumulator = sc.accumulator(0, "My Accumulator")
  var accumulatorArray = Array(2, 3, 4, 5)
  sc.parallelize(accumulatorArray).foreach(x => myAccumulator += x)
  myAccumulator.value
  println(myAccumulator.value)
}
